$(document).ready(function() {
    // nav menu actions
    $('.menu-toggler').on('click', function () {
        $(this).toggleClass('open');
        $('nav').toggleClass('open');
    });
    $('nav .nav-link').on('click', function () {
        $('.menu-toggler').removeClass('open');
        $('nav').removeClass('open');
    });

    // language handler
    $('#languages li').on('click', function () {
        const lang = $(this).attr('data-lang')
        switch (lang) {
            case 'arabic':
                $('body').addClass('rtl')
                $('#lang-flag').attr('src', '/assets/ae-flag.png')
            break;
            case 'english':
                $('body').removeClass('rtl')
                $('#lang-flag').attr('src', '/assets/uk-flag.png')
            break;
            case 'french':
                $('body').removeClass('rtl')
                $('#lang-flag').attr('src', '/assets/fr-flag.png')
            break;
        }
    })

    // mute and un mute video
    $('#mute-sound-btn').on('click', function () {
        $('#bg-vid').muted = true;
        $('#mute-sound-btn').hide();
        $('#unmute-sound-btn').show();
    });
    $('#unmute-sound-btn').on('click', function () {
        $('#bg-vid').muted = false;
        $('#unmute-sound-btn').hide();
        $('#mute-sound-btn').show();
    });

    // toggle small devices search bar
    $('#search-toggler').on('click', function () {
        console.log('in')
        $('#search-modal').fadeIn().addClass('visible-search');
    });
    $('#close-search-modal').on('click', function () {
        $('#search-modal').fadeOut().removeClass('visible-search');
    });

    // scroll navbar listner
    $(window).scroll(function(){
        $('nav').toggleClass('scrolled', $(this).scrollTop() > 300);
    });

    // gallery
    $('.gallery-menu ul li').click(function(){
        $('.gallery-menu ul li').removeClass('active');
        $(this).addClass('active');
        
        var selector = $(this).attr('data-filter');
        $('.gallery-item').isotope({
            filter:selector
        });
        return  false;
    });

    // calendar
    const springIcon = '<i class="fa-solid fa-fan"></i>';
    const summerIcon = '<i class="fa-regular fa-sun"></i>';
    const winterIcon = '<i class="fa-solid fa-cloud-rain"></i>';
    const fallIcon = '<i class="fa-solid fa-leaf"></i>';
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const currentYear = (new Date).getFullYear();
    let displayYear = (new Date).getFullYear();
    const currentMonth = months[(new Date).getMonth()];
    let displayMonth = months[(new Date).getMonth()];
    $('#year').text(displayYear);
    $('#month').text(displayMonth);

    $('#next-month').on('click', function () {
        if (months.indexOf(displayMonth) <  months.indexOf(months[months.length - 1])) {
            const currentMonthIndex = months.indexOf(displayMonth);
            displayMonth = months[currentMonthIndex + 1]
            $('#month').text(displayMonth);
        } else {
            displayMonth = months[0]
            $('#month').text(displayMonth);
            displayYear++
            $('#year').text(displayYear);
        }
        watchMonthChange();
    });

    $('#prev-month').on('click', function () {
        if (months.indexOf(displayMonth) >  months.indexOf(currentMonth) || displayYear > currentYear) {
            const currentMonthIndex = months.indexOf(displayMonth);
            if (displayYear > currentYear && displayMonth === months[0]) {
                displayYear--
                $('#year').text(displayYear);
                displayMonth = months[months.length - 1]
                $('#month').text(displayMonth);
            } else {
                displayMonth = months[currentMonthIndex - 1]
                $('#month').text(displayMonth);
            }
        } else {
            $('#prev-month').attr("disabled", true);
        }
        watchMonthChange();
    });

    function watchMonthChange () {
        if (displayMonth === currentMonth && displayYear === currentYear) $('#prev-month').attr("disabled", true);
        else $('#prev-month').attr("disabled", false)
        
        if (['December', 'January', 'February'].includes(displayMonth)) $('#season-icon').html(winterIcon);
        else if (['March', 'April', 'May'].includes(displayMonth)) $('#season-icon').html(springIcon);
        else if (['June', 'July', 'August'].includes(displayMonth)) $('#season-icon').html(summerIcon);
        else if (['September', 'October', 'November'].includes(displayMonth)) $('#season-icon').html(fallIcon);
    }

    watchMonthChange();
});
